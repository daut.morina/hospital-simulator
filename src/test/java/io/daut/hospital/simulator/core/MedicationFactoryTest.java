package io.daut.hospital.simulator.core;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MedicationFactoryTest {
    @Test
    public void testGetMedicationWithNoMedication() {
        final String medicationArgument = "";
        final Set<Medication> medication = MedicationFactory.createMedication(medicationArgument);

        assertTrue(medication.isEmpty());
    }

    @Test
    public void testGetMedicationWithParacetamol() {
        final String medicationArgument = "P";
        final Set<Medication> medication = MedicationFactory.createMedication(medicationArgument);

        assertEquals(1, medication.size());
        assertTrue(medication.contains(Medication.PARACETAMOL));
    }
}
