package io.daut.hospital.simulator.core;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class MedicationTest {

    @Test
    public void testByCodeWithValidCodeShouldBeSuccessful() {
        final String code = "As";
        final Medication medication = Medication.byCode(code);

        assertNotNull(medication);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testByCodeWithInvalidCodeShouldThrow() {
        final String code = "O";
        Medication.byCode(code);
    }
}
