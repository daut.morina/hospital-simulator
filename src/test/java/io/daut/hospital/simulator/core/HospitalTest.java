package io.daut.hospital.simulator.core;

import io.daut.hospital.simulator.core.patient.DeadPatient;
import io.daut.hospital.simulator.core.patient.HealthyPatient;
import io.daut.hospital.simulator.outbound.ConsoleRenderer;
import io.daut.hospital.simulator.outbound.FlyingSpaghettiMonster;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class HospitalTest {
    @Test
    public void testMedicatePatientsWithDiabeticPatientsAndWithoutMedicationShouldKillThem() {
        final Deity deity = new FlyingSpaghettiMonster(FlyingSpaghettiMonster.Graciousness.ACTUALLY_THIS_IS_NOT_BAD);
        final Renderer renderer = new ConsoleRenderer();
        final Hospital hospital = new Hospital(deity, renderer);

        final String patientsDTO = "D,D";
        final String medicationDTO = "";

        final String result = hospital.medicatePatients(patientsDTO, medicationDTO);
        final String expected = renderer.render(List.of(new DeadPatient(), new DeadPatient()));

        assertEquals(expected, result);
    }

    @Test
    public void testMedicatePatientsWithAFeverishPatientAndParacetamolShouldCureThem() {
        final Deity deity = new FlyingSpaghettiMonster(FlyingSpaghettiMonster.Graciousness.ACTUALLY_THIS_IS_NOT_BAD);
        final Renderer renderer = new ConsoleRenderer();
        final Hospital hospital = new Hospital(deity, renderer);

        final String patientsDTO = "F";
        final String medicationDTO = "P";

        final String result = hospital.medicatePatients(patientsDTO, medicationDTO);
        final String expected = renderer.render(List.of(new HealthyPatient()));

        assertEquals(expected, result);
    }

    @Test
    public void testMedicatePatientsWithDeadPatientAndAGraciousFlyingSpaghettiMonsterShouldReviveThem() {
        final Deity deity = new FlyingSpaghettiMonster(
                FlyingSpaghettiMonster.Graciousness.YOU_ARE_HEALTHY_YOU_ARE_HEALTHY_THIS_WHOLE_PROGRAM_IS_HEALTHY);
        final Renderer renderer = new ConsoleRenderer();
        final Hospital hospital = new Hospital(deity, renderer);

        final String patientsDTO = "X";
        final String medicationDTO = "";

        final String result = hospital.medicatePatients(patientsDTO, medicationDTO);
        final String expected = renderer.render(List.of(new HealthyPatient()));

        assertEquals(expected, result);
    }
}
