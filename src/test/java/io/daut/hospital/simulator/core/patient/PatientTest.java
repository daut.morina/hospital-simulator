package io.daut.hospital.simulator.core.patient;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Medication;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class PatientTest {
    @Test
    public void treatHealthyPatient_withAntibioticsAndInsulin_shouldEndUpFeverish() {
        treat(new HealthyPatient(),
                with(Medication.ANTIBIOTICS, Medication.INSULIN),
                andExpectThemToEndUp(Health.FEVERISH));
    }

    @Test
    public void treatHealthyPatient_withDeadlyCocktail_shouldEndUpDead() {
        treat(new HealthyPatient(),
                with(Medication.ASPIRIN, Medication.PARACETAMOL),
                andExpectThemToEndUp(Health.DEAD));
    }

    @Test
    public void treatHealthyPatient_withAFullCourse_shouldEndUpDead() {
        treat(new HealthyPatient(),
                with(Medication.ASPIRIN, Medication.ANTIBIOTICS, Medication.INSULIN, Medication.PARACETAMOL),
                andExpectThemToEndUp(Health.DEAD));
    }

    @Test
    public void treatDiabeticPatient_withDeadlyCocktail_shouldEndUpDead() {
        treat(new DiabeticPatient(),
                with(Medication.ASPIRIN, Medication.PARACETAMOL),
                andExpectThemToEndUp(Health.DEAD));
    }

    @Test
    public void treatDiabeticPatient_withJackAll_shouldEndUpDead() {
        treat(new DiabeticPatient(),
                with(),
                andExpectThemToEndUp(Health.DEAD));
    }

    @Test
    public void treatDiabeticPatient_withInsulin_shouldEndUpDiabetic() {
        treat(new DiabeticPatient(),
                with(Medication.INSULIN),
                andExpectThemToEndUp(Health.DIABETIC));
    }

    @Test
    public void treatFeverishPatient_withDeadlyCocktail_shouldEndUpDead() {
        treat(new FeverishPatient(),
                with(Medication.ASPIRIN, Medication.PARACETAMOL),
                andExpectThemToEndUp(Health.DEAD));
    }

    @Test
    public void treatFeverishPatient_withAspirin_shouldEndUpHealthy() {
        treat(new FeverishPatient(),
                with(Medication.ASPIRIN),
                andExpectThemToEndUp(Health.HEALTHY));
    }

    @Test
    public void treatFeverishPatient_withParacetamol_shouldEndUpHealthy() {
        treat(new FeverishPatient(),
                with(Medication.PARACETAMOL),
                andExpectThemToEndUp(Health.HEALTHY));
    }

    @Test
    public void treatTuberculousPatient_withDeadlyCocktail_shouldEndUpDead() {
        treat(new TuberculousPatient(),
                with(Medication.ASPIRIN, Medication.PARACETAMOL),
                andExpectThemToEndUp(Health.DEAD));
    }

    @Test
    public void treatTuberculousPatient_withAntibiotics_shouldEndUpHealthy() {
        treat(new TuberculousPatient(),
                with(Medication.ANTIBIOTICS),
                andExpectThemToEndUp(Health.HEALTHY));
    }

    private static void treat(Patient patientBeforeTreatment, Set<Medication> medication, Health healthAfterTreatment) {
        final Patient patientAfterTreatment = patientBeforeTreatment.treatWith(medication);
        assertEquals(healthAfterTreatment, patientAfterTreatment.getHealth());
    }

    private static Set<Medication> with(Medication... medications) {
        return Set.of(medications);
    }

    private static Health andExpectThemToEndUp(Health health) {
        return health;
    }
}
