package io.daut.hospital.simulator.core;

import io.daut.hospital.simulator.core.patient.Patient;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PatientFactoryTest {
    @Test
    public void testGetPatientsWithTwoDiabeticPatients() {
        final String patientsArgument = "D,D";
        final List<Patient> patients = PatientFactory.createPatients(patientsArgument);

        assertEquals(2, patients.size());
        assertEquals(Health.DIABETIC, patients.get(0).getHealth());
        assertEquals(Health.DIABETIC, patients.get(1).getHealth());
    }

    @Test
    public void testGetPatientsWithAFeverishPatient() {
        final String patientsArgument = "F";
        final List<Patient> patients = PatientFactory.createPatients(patientsArgument);

        assertEquals(1, patients.size());
        assertEquals(Health.FEVERISH, patients.get(0).getHealth());
    }
}
