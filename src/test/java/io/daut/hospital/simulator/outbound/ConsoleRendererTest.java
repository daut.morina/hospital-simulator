package io.daut.hospital.simulator.outbound;

import io.daut.hospital.simulator.core.Renderer;
import io.daut.hospital.simulator.core.patient.DiabeticPatient;
import io.daut.hospital.simulator.core.patient.Patient;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ConsoleRendererTest {
    @Test
    public void testPrint() {
        final List<Patient> patients = List.of(new DiabeticPatient(), new DiabeticPatient());
        final Renderer renderer = new ConsoleRenderer();
        final String result = renderer.render(patients);

        assertEquals("F:0,H:0,D:2,T:0,X:0", result);
    }
}
