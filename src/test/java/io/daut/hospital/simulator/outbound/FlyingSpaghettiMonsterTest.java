package io.daut.hospital.simulator.outbound;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.patient.DeadPatient;
import io.daut.hospital.simulator.core.patient.Patient;
import org.junit.Assert;
import org.junit.Test;

public class FlyingSpaghettiMonsterTest {

    @Test
    public void testReviveShouldReviveDeadPatient() {
        final FlyingSpaghettiMonster flyingSpaghettiMonster = new FlyingSpaghettiMonster(FlyingSpaghettiMonster.Graciousness.YOU_ARE_HEALTHY_YOU_ARE_HEALTHY_THIS_WHOLE_PROGRAM_IS_HEALTHY);
        final Patient deadPatient = new DeadPatient();
        final Patient luckyPatient = flyingSpaghettiMonster.revive(deadPatient);

        Assert.assertEquals(Health.HEALTHY, luckyPatient.getHealth());
    }
}
