package io.daut.hospital.simulator;

import io.daut.hospital.simulator.inbound.HospitalSimulatorCLI;

public class App {
    public static void main(String[] args) {
        final HospitalSimulatorCLI hospitalSimulatorCLI = new HospitalSimulatorCLI();
        hospitalSimulatorCLI.runSimulation(args);
    }
}
