package io.daut.hospital.simulator.inbound;

import io.daut.hospital.simulator.core.Deity;
import io.daut.hospital.simulator.core.Hospital;
import io.daut.hospital.simulator.core.Renderer;
import io.daut.hospital.simulator.outbound.ConsoleRenderer;
import io.daut.hospital.simulator.outbound.FlyingSpaghettiMonster;

public class HospitalSimulatorCLI {
    public void runSimulation(String[] args) {
        if (args.length != 2) throw new RuntimeException("Wrong number of arguments");

        final Deity deity = new FlyingSpaghettiMonster(FlyingSpaghettiMonster.Graciousness.REGULAR);
        final Renderer renderer = new ConsoleRenderer();
        final Hospital hospital = new Hospital(deity, renderer);

        final String result = hospital.medicatePatients(args[0], args[1]);

        System.out.println(result);
    }
}
