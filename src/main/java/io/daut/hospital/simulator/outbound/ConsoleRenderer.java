package io.daut.hospital.simulator.outbound;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Renderer;
import io.daut.hospital.simulator.core.patient.Patient;

import java.util.List;

public class ConsoleRenderer implements Renderer {
    public String render(List<Patient> patients) {
        String result = "";

        result = result + "F:" + getPatients(patients, Health.FEVERISH) + ",";
        result = result + "H:" + getPatients(patients, Health.HEALTHY) + ",";
        result = result + "D:" + getPatients(patients, Health.DIABETIC) + ",";
        result = result + "T:" + getPatients(patients, Health.TUBERCULOUS) + ",";

        return result + "X:" + getPatients(patients, Health.DEAD);
    }

    private static long getPatients(List<Patient> patients, Health health) {
        return patients.stream().filter(patient -> patient.getHealth().equals(health)).count();
    }
}
