package io.daut.hospital.simulator.outbound;

import io.daut.hospital.simulator.core.Deity;
import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.patient.HealthyPatient;
import io.daut.hospital.simulator.core.patient.Patient;

import java.util.concurrent.ThreadLocalRandom;

public class FlyingSpaghettiMonster implements Deity {
    public enum Graciousness {
        YOU_ARE_HEALTHY_YOU_ARE_HEALTHY_THIS_WHOLE_PROGRAM_IS_HEALTHY(1),
        REGULAR(1_000_000),
        ACTUALLY_THIS_IS_NOT_BAD(0);

        private final int grace;

        Graciousness(int grace) {
            this.grace = grace;
        }

        public int getGrace() {
            return grace;
        }
    }

    private final boolean gracious;

    public FlyingSpaghettiMonster(Graciousness graciousness) {
        if (graciousness == Graciousness.ACTUALLY_THIS_IS_NOT_BAD) {
            this.gracious = false;
        } else {
            this.gracious = 0 == ThreadLocalRandom.current().nextInt(0, graciousness.getGrace());
        }
    }

    public Patient revive(Patient patient) {
        if (patient.getHealth() == Health.DEAD && this.gracious) return new HealthyPatient();
        return patient;
    }
}
