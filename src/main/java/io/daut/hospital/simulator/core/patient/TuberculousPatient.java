package io.daut.hospital.simulator.core.patient;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Medication;

import java.util.Set;

public class TuberculousPatient extends Patient {

    public TuberculousPatient() {
        super(Health.TUBERCULOUS);
    }

    @Override
    public Patient treatWith(Set<Medication> medication) {
        if (isDeadlyCocktail(medication)) return new DeadPatient();
        if (medication.contains(Medication.ANTIBIOTICS)) return new HealthyPatient();

        return this;
    }
}
