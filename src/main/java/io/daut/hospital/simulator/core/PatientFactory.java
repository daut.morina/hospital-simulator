package io.daut.hospital.simulator.core;

import io.daut.hospital.simulator.core.patient.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PatientFactory {
    public static List<Patient> createPatients(String patientsArgument) {
        final String[] patientsCodes = patientsArgument.split(",");
        return Arrays.stream(patientsCodes)
                .map(PatientFactory::fromCode)
                .collect(Collectors.toList());
    }

    private static Patient fromCode(String code) {
        switch (code) {
            case "D":
                return new DiabeticPatient();
            case "F":
                return new FeverishPatient();
            case "H":
                return new HealthyPatient();
            case "T":
                return new TuberculousPatient();
            case "X":
                return new DeadPatient();
            default:
                throw new RuntimeException("Could not find health code '" + code + "'");
        }
    }
}
