package io.daut.hospital.simulator.core.patient;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Medication;

import java.util.Set;

public class HealthyPatient extends Patient {

    public HealthyPatient() {
        super(Health.HEALTHY);
    }

    @Override
    public Patient treatWith(Set<Medication> medication) {
        if (isDeadlyCocktail(medication)) return new DeadPatient();
        if (medication.containsAll(Set.of(Medication.INSULIN, Medication.ANTIBIOTICS))) return new FeverishPatient();

        return this;
    }
}
