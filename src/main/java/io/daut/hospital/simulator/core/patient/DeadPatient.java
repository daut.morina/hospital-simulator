package io.daut.hospital.simulator.core.patient;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Medication;

import java.util.Set;

public class DeadPatient extends Patient {

    public DeadPatient() {
        super(Health.DEAD);
    }

    @Override
    public Patient treatWith(Set<Medication> medication) {
        return this;
    }
}
