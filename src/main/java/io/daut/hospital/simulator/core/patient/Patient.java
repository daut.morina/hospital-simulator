package io.daut.hospital.simulator.core.patient;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Medication;

import java.util.Set;

public abstract class Patient {
    private Health health;

    public Patient(Health health) {
        this.health = health;
    }

    public abstract Patient treatWith(Set<Medication> medication);

    protected boolean isDeadlyCocktail(Set<Medication> medication) {
        return medication.containsAll(Set.of(Medication.ASPIRIN, Medication.PARACETAMOL));
    }

    public Health getHealth() {
        return health;
    }
}
