package io.daut.hospital.simulator.core;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class MedicationFactory {
    public static Set<Medication> createMedication(String medication) {
        final String[] medicationCodes = medication.split(",");
        return Arrays.stream(medicationCodes)
                .map(code -> {
                    try {
                        return Medication.byCode(code);
                    } catch (IllegalArgumentException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }
}
