package io.daut.hospital.simulator.core.patient;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Medication;

import java.util.Set;

public class DiabeticPatient extends Patient {

    public DiabeticPatient() {
        super(Health.DIABETIC);
    }

    @Override
    public Patient treatWith(Set<Medication> medication) {
        if (isDeadlyCocktail(medication)) return new DeadPatient();
        if (!medication.contains(Medication.INSULIN)) return new DeadPatient();

        return this;
    }
}
