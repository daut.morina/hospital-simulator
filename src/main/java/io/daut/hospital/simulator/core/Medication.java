package io.daut.hospital.simulator.core;

import java.util.Arrays;

public enum Medication {
    ANTIBIOTICS("An"),
    ASPIRIN("As"),
    INSULIN("I"),
    PARACETAMOL("P");

    private final String code;

    Medication(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Medication byCode(String code) {
        return Arrays.stream(Medication.values())
                .filter(medication -> medication.getCode().equals(code))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Could not find Medication with code '" + code + "'"));
    }
}
