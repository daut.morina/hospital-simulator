package io.daut.hospital.simulator.core;

import io.daut.hospital.simulator.core.patient.Patient;

public interface Deity {
    Patient revive(Patient patient);
}
