package io.daut.hospital.simulator.core;

import io.daut.hospital.simulator.core.patient.Patient;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Hospital {

    private final Deity deity;
    private final Renderer renderer;

    public Hospital(Deity deity, Renderer renderer) {
        this.deity = deity;
        this.renderer = renderer;
    }

    public String medicatePatients(String patientDTO, String medicationDTO) {
        final List<Patient> patients = PatientFactory.createPatients(patientDTO);
        final Set<Medication> medication = MedicationFactory.createMedication(medicationDTO);

        return patients
                .stream()
                .map(patient -> patient.treatWith(medication))
                .map(deity::revive)
                .collect(Collectors.collectingAndThen(Collectors.toList(), renderer::render));
    }
}
