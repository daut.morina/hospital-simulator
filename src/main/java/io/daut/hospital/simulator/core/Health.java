package io.daut.hospital.simulator.core;

public enum Health {
    HEALTHY,
    DIABETIC,
    FEVERISH,
    TUBERCULOUS,
    DEAD
}
