package io.daut.hospital.simulator.core.patient;

import io.daut.hospital.simulator.core.Health;
import io.daut.hospital.simulator.core.Medication;

import java.util.Set;

public class FeverishPatient extends Patient {

    public FeverishPatient() {
        super(Health.FEVERISH);
    }

    @Override
    public Patient treatWith(Set<Medication> medication) {
        if (isDeadlyCocktail(medication)) return new DeadPatient();
        if (medication.contains(Medication.ASPIRIN)) return new HealthyPatient();
        if (medication.contains(Medication.PARACETAMOL)) return new HealthyPatient();

        return this;
    }
}
