package io.daut.hospital.simulator.core;

import io.daut.hospital.simulator.core.patient.Patient;

import java.util.List;

public interface Renderer {
    String render(List<Patient> patients);
}
