# Hospital Simulator

## Setup
* Download / Clone the repository.
* `cd` into the directory
* Run `gradle build`
* Run an example like `gradle run --args='"F,H,D,T,X" "An,I,P"'` - One of each with Antibiotics, Insulin and Paracetamol

## Comments
* The first commit is a pretty straightforward solution - with a huge `switch`/`if` block
* The second commit improves design (tried to be a little hexagonal), naming (a little DDD), and leverages the type 
system a little better by having a type of each kind of Patient. I'm aware though that right now because of the `Health`
`enum` the example is not very extendable but I had to stop at some point. :)

Then there's also the usual things like logging and error handling that could be improved. 

I hope you guys will enjoy reviewing my variant and maybe you'll even like it. ;)
